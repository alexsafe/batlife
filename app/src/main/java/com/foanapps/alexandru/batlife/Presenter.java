package com.foanapps.alexandru.batlife;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by alexandru on 2/6/2017.
 */

public class Presenter {

    private Context mContext;
    private PreferenceManager ctrl;
    private float batLevel;
    private int percFreq;
    private int timeFreq;
    private long currentMilliTime;
    private boolean enabled;

    Presenter(Context context) {
        mContext = context;
        ctrl = new PreferenceManager(context);
        percFreq = ctrl.getPercentageSyncFrequency();
        timeFreq = ctrl.getTimeSyncFrequency() * 60;
        enabled = ctrl.getMasterEnabledStatus();
        batLevel = getBatteryLevel();
        currentMilliTime = System.currentTimeMillis() / 1000;
    }

    public static void appendLog(String text) {
        File logFile = new File("sdcard/log.json");
        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            // BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(text);
            buf.newLine();
            buf.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    boolean isCorrectSpeakPercentage() {
        if (percFreq != 0) {
            int diff = Math.abs((int) batLevel - ctrl.getLastBatLevel());
//            Log.d("test", "isCorrectSpeakPercentage impartire:" + diff % percFreq);

            if ((int) batLevel!=100 &&  (int) batLevel % 5 == 0) {

//                appendLog("isCorrectSpeakPercentage batLevel:" + batLevel);
//                appendLog("isCorrectSpeakPercentage percFreq:" + percFreq);
//                appendLog("isCorrectSpeakPercentage ctrl.getLastBatLevel():" + ctrl.getLastBatLevel());
//                appendLog("isCorrectSpeakPercentage diff:" + diff);
//                appendLog("isCorrectSpeakPercentage impartire:" + diff % percFreq);

            }

            if (diff >= percFreq && diff % percFreq == 0) {


//                Log.d("test", "isCorrectSpeakPercentage dupa primul if");
                if ((int) batLevel != ctrl.getLastBatLevel()) {
                    Log.d("test", "isCorrectSpeakPercentage ar trebui sa vb pt:" + batLevel);
                    appendLog("isCorrectSpeakPercentage ar trebui sa vb pt:" + batLevel+ " at "+currentMilliTime);
                    return true;
                }
            }
        }

        return false;
    }

    boolean isCorrectSpeakTime() {
        Log.d("test", "timecheck timeFreq:" + timeFreq);
        Log.d("Test", " isCorrectSpeakTime 0");
        if (timeFreq != 0) {
            int testTime = 2 * 60;
            Log.d("test", "timecheck testTime:" + testTime);
            Log.d("Test", " isCorrectSpeakTime 1");
            if (currentMilliTime - ctrl.getIniTime() >= timeFreq) {
                Log.d("Test", " isCorrectSpeakTime 2");
                appendLog("isCorrectSpeakTime ar trebui sa vb pt:" + batLevel);
//            if (currentMilliTime - testTime == timeFreq) {
                return true;
            }
        }

        return false;
    }

    boolean isBatLevel() {
        return enabled && batLevel <= 100;
    }

    float getBatteryLevel() {

        Intent batteryIntent = mContext.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
//        int health = batteryIntent.getIntExtra(BatteryManager.EXTRA_HEALTH, BatteryManager.BATTERY_HEALTH_UNKNOWN);

        if (level == -1 || scale == -1) {
            return 50.0f;
        }

        return ((float) level / (float) scale) * 100.0f;

    }

}
