package com.foanapps.alexandru.batlife;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.IBinder;
import android.speech.tts.TextToSpeech;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

public class BatNotifications extends Service {

    private static final int MAX_STREAMS = 2;
    private static final int streamType = AudioManager.STREAM_MUSIC;
    private static AudioManager audioManager;

    Timer timer;
    MyTimerTask myTimerTask = new MyTimerTask();
    int timeFreq;
    float volume;
    private boolean loaded;
    private SoundPool soundPool;
    private int ten, twenty, thirty, fourty, fifty, sixty, seventy, eighty, ninty;
    private BroadcastReceiver mBatInfoReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context arg0, Intent intent) {
            int level = intent.getIntExtra("level", 0);

            Log.d("test", "bat level:" + level);
            // TODO: Preform action based upon battery level
        }
    };


    public BatNotifications() {
        Log.d("test", "servoce comstrittpr");

        final String PREF_FILE_NAME = "PrefFile";

        if (timer != null) {
            timer.cancel();
        }
        timer = new Timer();
        timer.schedule(myTimerTask, 1000, 1000);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d("test", "binded");
        registerReceiver(this.mBatInfoReceiver,
                new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }


    public float getBatteryLevel() {
        Intent batteryIntent = registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        int health = batteryIntent.getIntExtra(BatteryManager.EXTRA_HEALTH, BatteryManager.BATTERY_HEALTH_UNKNOWN);

        // Error checking that probably isn't needed but I added just in case.
        if (level == -1 || scale == -1) {
            return 50.0f;
        }

        return ((float) level / (float) scale) * 100.0f;
    }


    class MyTimerTask extends TimerTask implements TextToSpeech.OnInitListener, TextToSpeech.OnUtteranceCompletedListener {

        TextToSpeech textToSpeech;

        @Override
        public void run() {

            Presenter appInfo = new Presenter(getApplicationContext());
            final PreferenceManager ctrl = new PreferenceManager(getApplicationContext());

            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd:MMMM:yyyy HH:mm:ss a");
            SimpleDateFormat simpleDateMinute = new SimpleDateFormat("mm");
            final String strDate = simpleDateFormat.format(calendar.getTime());
            int intDateMinute = Integer.valueOf(simpleDateMinute.format(calendar.getTime()));

            long currentMilliTime = System.currentTimeMillis() / 1000;

            textToSpeech = new TextToSpeech(getApplicationContext(), this);
//            textToSpeech.setLanguage(new Locale("fr-FR"));
            final float batLevel = appInfo.getBatteryLevel();


            int percFreq = ctrl.getPercentageSyncFrequency();
            int timeFreq = ctrl.getTimeSyncFrequency() * 60;
            boolean enabled = ctrl.getMasterEnabledStatus();


            if (appInfo.isBatLevel()) {
                if (appInfo.isCorrectSpeakPercentage()) {
                    Log.d("Test", "batLevel isCorrectSpeakPercentage");
                    ctrl.setLastBatLevel((int) batLevel);

                    new AsyncTask<Void, Void, Void>() {

                        @Override
                        protected Void doInBackground(Void... params) {

                            speak(batLevel);
                            return null;
                        }
                    }.execute((Void) null);
                }

                if (appInfo.isCorrectSpeakTime()) {
                    Log.d("Test", "batLevel isCorrectSpeakTime");
                    Log.d("Test", "tospeak:");
                    ctrl.setIniTime(System.currentTimeMillis() / 1000);
                    new AsyncTask<Void, Void, Void>() {

                        @Override
                        protected Void doInBackground(Void... params) {
                            Log.d("Test", "tospeak: in async");
                            speak(batLevel);
                            return null;
                        }
                    }.execute((Void) null);
                }
            }
        }

        @Override
        public void onInit(int status) {
            textToSpeech.setOnUtteranceCompletedListener(this);
//            speak();
        }

        @Override
        public void onUtteranceCompleted(String arg0) {
            Log.d("test", "a zis");
//            int streamId = soundPool.play(ten, leftVolumn, rightVolumn, 1, 0, 1f);
            Log.d("test", "a zis a doua parte");
        }

        private void speak(float batteryLevel) {

            HashMap<String, String> params = new HashMap<String, String>();
            params.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "meaninglessString");
            String level = Integer.toString((int) batteryLevel);

            AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            int amStreamMusicMaxVol = am.getStreamMaxVolume(am.STREAM_MUSIC);
            int volume_level = am.getStreamVolume(AudioManager.STREAM_MUSIC);
            float leftVolumn = 14.0f;//BatNotifications.this.volume;
            float volume = BatNotifications.this.volume;
            am.setStreamVolume(am.STREAM_MUSIC, volume_level, 0);


            String[] locales = Resources.getSystem().getAssets().getLocales();
            textToSpeech.speak("Battery level at:" + level + " percent ", TextToSpeech.QUEUE_FLUSH, params);

            Presenter.appendLog("isCorrectSpeakPercentage spoke:" + level);

        }
    }
}


