package com.foanapps.alexandru.batlife;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by alexandru on 3/7/2017.
 */

public class BootUpReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        /****** For Start Activity *****/
        Intent i = new Intent(context, SettingsActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);

        /***** For start Service  ****/
//        Intent myIntent = new Intent(context, MyService.class);
//        context.startService(myIntent);
    }

}
