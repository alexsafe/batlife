package com.foanapps.alexandru.batlife;


import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.os.BatteryManager;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.support.v7.app.ActionBar;
import android.util.Log;

import alobar.preference.NumberPickerPreference;

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p/>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class SettingsActivity extends AppCompatPreferenceActivity {
    private static final int MAX_STREAMS = 5;
    private static final int streamType = AudioManager.STREAM_MUSIC;
    ListPreference listTimePref;
    alobar.preference.NumberPickerPreference listPercentagePref;
    PreferenceManager ctrl;
    /**
     * A preference value change listener that updates the preference's summary
     * to reflect its new value.
     */

    private Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object value) {

            String stringValue = value.toString();

            Log.d("test", "preference :" + stringValue);
            listTimePref = (ListPreference) findPreference("time_sync_frequency");
            listPercentagePref = (alobar.preference.NumberPickerPreference) findPreference("percentage_sync_frequency");

            if (preference instanceof ListPreference) {
                // For list preferences, look up the correct display value in
                // the preference's 'entries' list.
                ListPreference listPreference = (ListPreference) preference;
                int index = listPreference.findIndexOfValue(stringValue);
                // Set the summary to reflect the new value.
                preference.setSummary(index >= 0 ? listPreference.getEntries()[index] : null);


                ctrl.setIniTime(System.currentTimeMillis() / 1000);
                ctrl.setTimeSyncFrequency(stringValue);

                if (preference.getKey().equals("time_sync_frequency") && index >= 0) {
                    if (!listPreference.getEntryValues()[index].equals("0")) {
                        listPercentagePref.setValue(0);
                        listPercentagePref.setSummary("Never");
                        ctrl.setPercentageSyncFrequency(0);
                    }
                }


            }
            if (preference instanceof alobar.preference.NumberPickerPreference) {
                NumberPickerPreference listPreference = (NumberPickerPreference) preference;
                Presenter appInfo = new Presenter(getApplicationContext());
//                int index = listPreference. listPreference.findIndexOfValue(stringValue);
                int percValue = listPreference.getValue();
                int val = 0;
                Log.d("test", "begin get val:");
                if (ctrl.getFirstStart()) {
                    ctrl.setPercentageSyncFrequency(5);
                    ctrl.setFirstStart(false);
                    val = 5;
                    Log.d("test", "begin get val:" + appInfo.getBatteryLevel());
                    ctrl.setLastBatLevel((int) appInfo.getBatteryLevel());
                } else {
                    val = Integer.parseInt(String.valueOf(5 + ((int) value * 5)));
                    ctrl.setLastBatLevel((int) appInfo.getBatteryLevel());
                }

                // Set the summary to reflect the new value.


                Log.d("test", "val:" + (int) appInfo.getBatteryLevel());
                ctrl.setPercentageSyncFrequency(val);
                preference.setSummary(val > 0 ? val + "%" : "Never");
                if (preference.getKey().equals("percentage_sync_frequency")) {
                    Log.d("test", "val diferit");
                    if (val != 0) {
                        listTimePref.setValue("0");
                        listTimePref.setSummary("Never");
                        ctrl.setTimeSyncFrequency("0");
                    }
                }

            }
            return true;
        }
    };


    /**
     * Helper method to determine if the device has an extra-large screen. For
     * example, 10" tablets are extra-large.
     */
    private static boolean isXLargeTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_XLARGE;
    }

    /**
     * Binds a preference's summary to its value. More specifically, when the
     * preference's value is changed, its summary (line of text below the
     * preference title) is updated to reflect the value. The summary is also
     * immediately updated upon calling this method. The exact display format is
     * dependent on the type of preference.
     *
     * @see #sBindPreferenceSummaryToValueListener
     */

    public static SharedPreferences getSharedPreferencess(Context ctxt) {
        final String PREF_FILE_NAME = "PrefFile";
        return ctxt.getSharedPreferences("FILE", 0);
    }

    private void bindStringPreferenceSummaryToValue(Preference preference) {
        // Set the listener to watch for value changes.
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);
//        SharedPreferences shareprefs = MyApplication.preferences;
        sBindPreferenceSummaryToValueListener.onPreferenceChange(preference, ctrl.getTimeSyncFrequency());

    }

    private void bindIntPreferenceSummaryToValue(Preference preference) {
        // Set the listener to watch for value changes.
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);
        sBindPreferenceSummaryToValueListener.onPreferenceChange(preference, ctrl.getPercentageSyncFrequency());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupActionBar();
        ctrl = new PreferenceManager(getApplicationContext());
        addPreferencesFromResource(R.xml.pref_data_sync);
        ctrl.setFalseBatLevel(100);
        // Bind the summaries of EditText/List/Dialog/Ringtone preferences
        // to their values. When their values change, their summaries are
        // updated to reflect the new value, per the Android Design
        // guidelines.
//        bindPreferenceSummaryToValue(findPreference("example_switch"));
        bindStringPreferenceSummaryToValue(findPreference("time_sync_frequency"));
        bindIntPreferenceSummaryToValue(findPreference("percentage_sync_frequency"));
    }

    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(false);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onIsMultiPane() {
        return isXLargeTablet(this);
    }


    public float getBatteryLevel() {
        Intent batteryIntent = registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        int health = batteryIntent.getIntExtra(BatteryManager.EXTRA_HEALTH, BatteryManager.BATTERY_HEALTH_UNKNOWN);

        // Error checking that probably isn't needed but I added just in case.
        if (level == -1 || scale == -1) {
            return 50.0f;
        }

        return ((float) level / (float) scale) * 100.0f;
    }

}