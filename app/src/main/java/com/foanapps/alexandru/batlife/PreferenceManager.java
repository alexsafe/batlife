package com.foanapps.alexandru.batlife;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by alexandru on 1/22/2017.
 */

class PreferenceManager {
    //    private Context mContext;
    private SharedPreferences preferences;

    PreferenceManager(Context context) {
//        mContext=context;
        preferences = context.getSharedPreferences(context.getPackageName() + "_preferences", MODE_PRIVATE);
    }

    int getPercentageSyncFrequency() {
        int level=preferences.getInt("percentage_sync_freq", 10);
        Log.d("test","percsync getPercentageSyncFrequency:"+level);
        return level;
    }

    void setPercentageSyncFrequency(int value) {
        Log.d("test","percsync setPercentageSyncFrequency:"+value);
        preferences.edit().putInt("percentage_sync_freq", value).apply();
    }

    int getTimeSyncFrequency() {
        String level=preferences.getString("time_sync_frequency", "10");
        Log.d("test","percsync getTimeSyncFrequency:"+level);
        return Integer.parseInt(preferences.getString("time_sync_frequency", "10"));
    }

    void setTimeSyncFrequency(String value) {
        Log.d("test","percsync setTimeSyncFrequency:"+value);
        preferences.edit().putString("time_sync_frequency", value).apply();
    }

    boolean getMasterEnabledStatus() {
        return preferences.getBoolean("example_switch", true);
    }

    int getLastBatLevel() {
        return preferences.getInt("last_level", 100);
    }

    void setLastBatLevel(int level) {
        preferences.edit().putInt("last_level", level).apply();
    }

    long getIniTime() {
        return preferences.getLong("time_stamp", 100);
    }

    void setIniTime(long time) {
        preferences.edit().putLong("time_stamp", time).apply();
    }

    boolean getFirstStart(){
        Log.d("test","firststart get:"+preferences.getBoolean("first_start", true));
        return preferences.getBoolean("first_start", true);
    }

    void setFirstStart(boolean start){
        Log.d("test","firststart set:"+start);
        preferences.edit().putBoolean("first_start", start).apply();
    }

    void setFalseBatLevel(int level){
        Log.d("test","set false_bat_level set:"+level);
        preferences.edit().putInt("false_bat_level", level).apply();
    }

    int getFalseBatLevel(){
        int level=preferences.getInt("false_bat_level",100);
        Log.d("test","get false_bat_level set:"+level);
        return level;
    }

}
