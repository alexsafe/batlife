package alobar.preference;

import android.content.Context;
import android.content.res.TypedArray;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.NumberPicker;

/**
 * A {@link android.preference.Preference} that displays a number picker as a dialog.
 */
public class NumberPickerPreference extends DialogPreference {

    public static final int DEFAULT_MAX_VALUE = 95;
    public static final int DEFAULT_MIN_VALUE = 5;
    public static final boolean DEFAULT_WRAP_SELECTOR_WHEEL = true;
    private final boolean wrapSelectorWheel;
    private final int minValue;
    private final int maxValue;
    private NumberPicker picker;
    private int value;
    private int defaultValue;

    public NumberPickerPreference(Context context, AttributeSet attrs) {
        this(context, attrs, android.R.attr.dialogPreferenceStyle);
    }

    public NumberPickerPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.NumberPickerPreference);
        minValue = a.getInteger(R.styleable.NumberPickerPreference_minValue, DEFAULT_MIN_VALUE);
        maxValue = a.getInteger(R.styleable.NumberPickerPreference_maxValue, DEFAULT_MAX_VALUE);
        defaultValue = a.getInteger(R.styleable.NumberPickerPreference_defaultValue, DEFAULT_MIN_VALUE);
        wrapSelectorWheel = a.getBoolean(R.styleable.NumberPickerPreference_wrapSelectorWheel, DEFAULT_WRAP_SELECTOR_WHEEL);
        a.recycle();
    }

    @Override
    protected View onCreateDialogView() {
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.gravity = Gravity.CENTER;

        picker = new NumberPicker(getContext());
        picker.setLayoutParams(layoutParams);
//        picker.setValue(0);
        FrameLayout dialogView = new FrameLayout(getContext());
        dialogView.addView(picker);

        return dialogView;
    }

    @Override
    protected void onBindDialogView(View view) {
        super.onBindDialogView(view);

        int length = maxValue / minValue;
//        int length = maxValue - minValue ;
        Log.d("test", "min val:" + minValue + " max:" + maxValue);
        String[] minuteValues = new String[length];
        int ind = 1;
        for (int i = minValue; i < maxValue; i++) {
            if (i % 5 == 0) {
                Log.d("test", "increments: i=" + i + " index:" + ind);
                String number = Integer.toString(i);
                minuteValues[ind] = number;
                ind++;
            }
        }
        String[] myValues = getArrayWithSteps(minValue, maxValue, 5);
        NumberPicker.Formatter formatter = new NumberPicker.Formatter() {
            @Override
            public String format(int value) {
                int temp = value * 5;
                return "" + temp;
            }
        };
//        picker.setFormatter(formatter);
//        Log.d("test","increments last val: "+minuteValues[18]);
//        Log.d("test","increments last length: "+minuteValues.length);
//        Log.d("test","increments last index: "+index);

        picker.setWrapSelectorWheel(wrapSelectorWheel);
//        picker.setMinValue(0);
        Log.d("test", "getValue:" + picker.getValue());

//        picker.setValue(defaultValue);
//        picker.setMaxValue(maxValue);
        picker.setMaxValue((maxValue - 5) / minValue);
        picker.setDisplayedValues(myValues);
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        if (positiveResult) {
            picker.clearFocus();
            int newValue = picker.getValue();
            if (callChangeListener(newValue)) {
                setValue(newValue);
            }
        }
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        return a.getInt(index, minValue);
    }

    @Override
    protected void onSetInitialValue(boolean restorePersistedValue, Object defaultValue) {
        setValue(restorePersistedValue ? getPersistedInt(minValue) : (Integer) defaultValue);
    }

    public int getValue() {
        return this.value;
    }

    public void setValue(int value) {
        this.value = value;
        persistInt(this.value);
        notifyChanged();
    }

    public String[] getArrayWithSteps(int iMinValue, int iMaxValue, int iStep) {
//        int iStepsArray = (iMaxValue - iMinValue) / iStep + 1; //get the lenght array that will return
        int iStepsArray = (iMaxValue / iMinValue); //get the lenght array that will return
        String[] arrayValues = new String[iStepsArray]; //Create array with length of iStepsArray
        for (int i = 0; i < iStepsArray; i++) {
            arrayValues[i] = String.valueOf(iMinValue + (i * iStep));
        }
        Log.d("test", "iMaxValue:" + iMaxValue + " min:" + iMinValue + "len:" + iStepsArray);
        for (int i = 0; i < arrayValues.length; i++) {
            Log.d("test", "i:" + i + "  val:" + arrayValues[i]);
        }

        return arrayValues;
    }
}
